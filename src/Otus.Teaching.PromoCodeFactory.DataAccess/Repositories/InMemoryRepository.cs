﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            
            ((List<T>)Data).Add(entity);
            
            return Task.FromResult(Data.Last());
        }

        public Task<T> UpdateAsync(T entity)
        {
            var entityToUpdate = Data.FirstOrDefault(e => e.Id == entity.Id);
            if (entityToUpdate != null)
            {
                int index = ((List<T>)Data).IndexOf(entityToUpdate);
                ((List<T>)Data)[index] = entity;
            }

            return Task.FromResult(entityToUpdate);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var entityToDelete = Data.FirstOrDefault(e => e.Id == id);
            if (entityToDelete != null)
            {
                int index = ((List<T>)Data).IndexOf(entityToDelete);
                ((List<T>)Data).RemoveAt(index);
            }
            
            return Task.FromResult(true);
        }
    }
}