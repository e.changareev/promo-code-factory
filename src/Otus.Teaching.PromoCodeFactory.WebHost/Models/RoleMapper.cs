﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public static class RoleMapper
    {
        public static Role ConvertToRole(RoleItemResponse roleItem)
        {
            return new Role
            {
                Id = roleItem.Id, 
                Name = roleItem.Name,
                Description = roleItem.Description
            };
        }
    }
}