﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public static class EmployeeMapper
    {
        public static Employee CreateEmployee(CreateEmployeeModel input)
        {
            return new Employee
            {
                FirstName = input.FirstName,
                LastName = input.LastName,
                Email = input.Email,
                Roles = input.Roles.Select(r => RoleMapper.ConvertToRole(r)).ToList(),
                AppliedPromocodesCount = input.AppliedPromocodesCount
            };
        }

        public static Employee UpdateEmployee(UpdateEmployeeModel input)
        {
            return new Employee
            {
                Id = input.Id,
                FirstName = input.FirstName,
                LastName = input.LastName,
                Email = input.Email,
                Roles = input.Roles.Select(r => RoleMapper.ConvertToRole(r)).ToList(),
                AppliedPromocodesCount = input.AppliedPromocodesCount
            };
        }
    }
}
